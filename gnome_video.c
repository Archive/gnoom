//-----------------------------------------------------------------------------
//
//	This source module provides GNOME compatibility with Doom
//
//		(c) 1997 Alan Cox
//
//-----------------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdktypes.h>
#include <gdk/gdkkeysyms.h>
#include <gnome.h>

#include <stdarg.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <errnos.h>
#include <signal.h>

#include "doomstat.h"
#include "i_system.h"
#include "v_video.h"
#include "m_argv.h"
#include "d_main.h"

#include "doomdef.h"

#define POINTER_WARP_COUNTDOWN	1

GtkWidget	*main_window=NULL;
GtkWidget	*drawing_window=NULL;
GdkImage	*drawing_image=NULL;
GdkGC		*drawing_gc=NULL;
int		X_width=320;
int		X_height=200;


// Fake mouse handling.
// This cannot work properly w/o DGA.
// Needs an invisible mouse cursor at least.
boolean		grabMouse;
int		doPointerWarp = POINTER_WARP_COUNTDOWN;

// Blocky mode,
// replace each 320x200 pixel with multiply*multiply pixels.
// According to Dave Taylor, it still is a bonehead thing
// to use ....

static int	multiply=1;

// Are we active

static int 	running=0;

static GdkColor	colors[256];
static GdkColormap *private_colourmap;

unsigned long rgb16[256];
unsigned int rgb24[256];
unsigned int rgb32[256];

//
//  Translates the key currently in X_event
//


int xlatekey(GdkEvent *event)
{
	int rc;
	switch(rc=event->key.keyval)
	{
		case GDK_Left:
			rc = KEY_LEFTARROW;
			break;
		case GDK_Right:
			rc = KEY_RIGHTARROW;
			break;
		case GDK_Down:
			rc = KEY_DOWNARROW;
			break;
		case GDK_Up:
			rc = KEY_UPARROW;
			break;
		case GDK_Escape:
	      		rc = KEY_ESCAPE;
	      		break;
		case GDK_Return:
	      		rc = KEY_ENTER;	
	      		break;
		case GDK_Tab:
	      		rc = KEY_TAB;
	      		break;
		case GDK_F1:	rc = KEY_F1;		break;
		case GDK_F2:	rc = KEY_F2;		break;
		case GDK_F3:	rc = KEY_F3;		break;
		case GDK_F4:	rc = KEY_F4;		break;
		case GDK_F5:	rc = KEY_F5;		break;
		case GDK_F6:	rc = KEY_F6;		break;
		case GDK_F7:	rc = KEY_F7;		break;
		case GDK_F8:	rc = KEY_F8;		break;
		case GDK_F9:	rc = KEY_F9;		break;
		case GDK_F10:	rc = KEY_F10;		break;
		case GDK_F11:	rc = KEY_F11;		break;
		case GDK_F12:	rc = KEY_F12;		break;
		case GDK_BackSpace:
		case GDK_Delete:
			rc = KEY_BACKSPACE;
			break;
		case GDK_Pause:
			rc = KEY_PAUSE;
			break;
		case GDK_KP_Equal:
		case GDK_equal:	
			rc = KEY_EQUALS;
			break;
		case GDK_KP_Subtract:
		case GDK_minus:	
			rc = KEY_MINUS;
			break;
		case GDK_Shift_L:
		case GDK_Shift_R:
			rc = KEY_RSHIFT;
			break;
		case GDK_Control_L:
		case GDK_Control_R:
			rc = KEY_RCTRL;
			break;
		case GDK_Alt_L:
		case GDK_Meta_L:
		case GDK_Alt_R:
		case GDK_Meta_R:
			rc = KEY_RALT;
			break;
	
		default:
			if (rc >= GDK_space && rc <= GDK_asciitilde)
				rc = rc - GDK_space + ' ';
			if (rc >= 'A' && rc <= 'Z')
				rc = rc - 'A' + 'a';
			break;
	}
	return rc;
}

void I_ShutdownGraphics(void)
{
	;
}



//
// I_StartFrame
//
void I_StartFrame (void)
{
    // er?

}

static int	lastmousex = 0;
static int	lastmousey = 0;
boolean		mousemoved = false;
boolean		shmFinished;

void I_GetEvent(void)
{
#if 0
    event_t event;

    // put event-grabbing stuff in here
    XNextEvent(X_display, &X_event);
    switch (X_event.type)
    {
      case KeyPress:
	event.type = ev_keydown;
	event.data1 = xlatekey();
	D_PostEvent(&event);
	// fprintf(stderr, "k");
	break;
      case KeyRelease:
	event.type = ev_keyup;
	event.data1 = xlatekey();
	D_PostEvent(&event);
	// fprintf(stderr, "ku");
	break;
      case ButtonPress:
	event.type = ev_mouse;
	event.data1 =
	    (X_event.xbutton.state & Button1Mask)
	    | (X_event.xbutton.state & Button2Mask ? 2 : 0)
	    | (X_event.xbutton.state & Button3Mask ? 4 : 0)
	    | (X_event.xbutton.button == Button1)
	    | (X_event.xbutton.button == Button2 ? 2 : 0)
	    | (X_event.xbutton.button == Button3 ? 4 : 0);
	event.data2 = event.data3 = 0;
	D_PostEvent(&event);
	// fprintf(stderr, "b");
	break;
      case ButtonRelease:
	event.type = ev_mouse;
	event.data1 =
	    (X_event.xbutton.state & Button1Mask)
	    | (X_event.xbutton.state & Button2Mask ? 2 : 0)
	    | (X_event.xbutton.state & Button3Mask ? 4 : 0);
	// suggest parentheses around arithmetic in operand of |
	event.data1 =
	    event.data1
	    ^ (X_event.xbutton.button == Button1 ? 1 : 0)
	    ^ (X_event.xbutton.button == Button2 ? 2 : 0)
	    ^ (X_event.xbutton.button == Button3 ? 4 : 0);
	event.data2 = event.data3 = 0;
	D_PostEvent(&event);
	// fprintf(stderr, "bu");
	break;
      case MotionNotify:
	event.type = ev_mouse;
	event.data1 =
	    (X_event.xmotion.state & Button1Mask)
	    | (X_event.xmotion.state & Button2Mask ? 2 : 0)
	    | (X_event.xmotion.state & Button3Mask ? 4 : 0);
	event.data2 = (X_event.xmotion.x - lastmousex) << 2;
	event.data3 = (lastmousey - X_event.xmotion.y) << 2;

	if (event.data2 || event.data3)
	{
	    lastmousex = X_event.xmotion.x;
	    lastmousey = X_event.xmotion.y;
	    if (X_event.xmotion.x != X_width/2 &&
		X_event.xmotion.y != X_height/2)
	    {
		D_PostEvent(&event);
		// fprintf(stderr, "m");
		mousemoved = false;
	    } else
	    {
		mousemoved = true;
	    }
	}
	break;
	
      case Expose:
      case ConfigureNotify:
	break;
	
      default:
	if (doShm && X_event.type == X_shmeventtype) shmFinished = true;
	break;
    }
#endif    

}

#if 0
Cursor createnullcursor
( Display*	display,
  Window	root )
{
//
//	Write me ;)
//
}
#endif

//
// I_StartTic
//
//	Start of a time tick. Empty any pending events through gtk_main_iteration
//	(I hope thats right) then set things going. 
//

void I_StartTic (void)
{

    while (gtk_events_pending())
    	gtk_main_iteration();
   
    if (!running)
	return;

    // Warp the pointer back to the middle of the window
    //  or it will wander off - that is, the game will
    //  loose input focus within X11.
#if 0    
    if (grabMouse)
    {
	if (!--doPointerWarp)
	{
	    XWarpPointer( X_display,
			  None,
			  X_mainWindow,
			  0, 0,
			  0, 0,
			  X_width/2, X_height/2);

	    doPointerWarp = POINTER_WARP_COUNTDOWN;
	}
    }
#endif    

    mousemoved = false;

}


//
// I_UpdateNoBlit
//
void I_UpdateNoBlit (void)
{
    // what is this?
}

//
// I_FinishUpdate
//

/*
 *	Called at the end of a rendering of a frame.
 */
void I_FinishUpdate (void)
{
    static int	lasttic;
    int		tics;
    int		i;
    // UNUSED static unsigned char *bigscreen=0;

    // draws little dots on the bottom of the screen
    if (devparm)
    {

	i = I_GetTime();
	tics = i - lasttic;
	lasttic = i;
	if (tics > 20) tics = 20;

	for (i=0 ; i<tics*2 ; i+=2)
	    screens[0][ (SCREENHEIGHT-1)*SCREENWIDTH + i] = 0xff;
	for ( ; i<20*2 ; i+=2)
	    screens[0][ (SCREENHEIGHT-1)*SCREENWIDTH + i] = 0x0;
    
    }

#if 0
    // scales the screen size before blitting it
    if (multiply == 2)
    {
	unsigned int *olineptrs[2];
	unsigned int *ilineptr;
	int x, y, i;
	unsigned int twoopixels;
	unsigned int twomoreopixels;
	unsigned int fouripixels;

	ilineptr = (unsigned int *) (screens[0]);
	for (i=0 ; i<2 ; i++)
	    olineptrs[i] = (unsigned int *) &image->data[i*X_width];

	y = SCREENHEIGHT;
	while (y--)
	{
	    x = SCREENWIDTH;
	    do
	    {
		fouripixels = *ilineptr++;
		twoopixels =	(fouripixels & 0xff000000)
		    |	((fouripixels>>8) & 0xffff00)
		    |	((fouripixels>>16) & 0xff);
		twomoreopixels =	((fouripixels<<16) & 0xff000000)
		    |	((fouripixels<<8) & 0xffff00)
		    |	(fouripixels & 0xff);
#ifdef __BIG_ENDIAN__
		*olineptrs[0]++ = twoopixels;
		*olineptrs[1]++ = twoopixels;
		*olineptrs[0]++ = twomoreopixels;
		*olineptrs[1]++ = twomoreopixels;
#else
		*olineptrs[0]++ = twomoreopixels;
		*olineptrs[1]++ = twomoreopixels;
		*olineptrs[0]++ = twoopixels;
		*olineptrs[1]++ = twoopixels;
#endif
	    } while (x-=4);
	    olineptrs[0] += X_width/4;
	    olineptrs[1] += X_width/4;
	}

    }
    else if (multiply == 3)
    {
	unsigned int *olineptrs[3];
	unsigned int *ilineptr;
	int x, y, i;
	unsigned int fouropixels[3];
	unsigned int fouripixels;

	ilineptr = (unsigned int *) (screens[0]);
	for (i=0 ; i<3 ; i++)
	    olineptrs[i] = (unsigned int *) &image->data[i*X_width];

	y = SCREENHEIGHT;
	while (y--)
	{
	    x = SCREENWIDTH;
	    do
	    {
		fouripixels = *ilineptr++;
		fouropixels[0] = (fouripixels & 0xff000000)
		    |	((fouripixels>>8) & 0xff0000)
		    |	((fouripixels>>16) & 0xffff);
		fouropixels[1] = ((fouripixels<<8) & 0xff000000)
		    |	(fouripixels & 0xffff00)
		    |	((fouripixels>>8) & 0xff);
		fouropixels[2] = ((fouripixels<<16) & 0xffff0000)
		    |	((fouripixels<<8) & 0xff00)
		    |	(fouripixels & 0xff);
#ifdef __BIG_ENDIAN__
		*olineptrs[0]++ = fouropixels[0];
		*olineptrs[1]++ = fouropixels[0];
		*olineptrs[2]++ = fouropixels[0];
		*olineptrs[0]++ = fouropixels[1];
		*olineptrs[1]++ = fouropixels[1];
		*olineptrs[2]++ = fouropixels[1];
		*olineptrs[0]++ = fouropixels[2];
		*olineptrs[1]++ = fouropixels[2];
		*olineptrs[2]++ = fouropixels[2];
#else
		*olineptrs[0]++ = fouropixels[2];
		*olineptrs[1]++ = fouropixels[2];
		*olineptrs[2]++ = fouropixels[2];
		*olineptrs[0]++ = fouropixels[1];
		*olineptrs[1]++ = fouropixels[1];
		*olineptrs[2]++ = fouropixels[1];
		*olineptrs[0]++ = fouropixels[0];
		*olineptrs[1]++ = fouropixels[0];
		*olineptrs[2]++ = fouropixels[0];
#endif
	    } while (x-=4);
	    olineptrs[0] += 2*X_width/4;
	    olineptrs[1] += 2*X_width/4;
	    olineptrs[2] += 2*X_width/4;
	}

    }
    else if (multiply == 4)
    {
	// Broken. Gotta fix this some day.
	void Expand4(unsigned *, double *);
  	Expand4 ((unsigned *)(screens[0]), (double *) (image->data));
    }
#endif

    /*
     *		Turn 8 bit colour data into real 16bit colour
     *		(alone for now)
     */

    {
    	int x,y;
    	unsigned short *dptr=drawing_image->mem;
    	unsigned char *sptr=screens[0];
    	
    	/* If there is an asm candidate its probably this one */

	if(multiply==4)
	{
		unsigned long *dptrh=(unsigned long *)dptr;
		unsigned long *dptrhm=dptrh+SCREENWIDTH*2;
		unsigned long *dptrlm=dptrhm+SCREENWIDTH*2;
		unsigned long *dptrl=dptrlm+SCREENWIDTH*2;
		for(y=0;y<SCREENHEIGHT;y++)
		{
			for(x=0;x<SCREENWIDTH;x++)
			{
    				*dptrh++=rgb16[*sptr];	
    				*dptrh++=rgb16[*sptr];	
    				*dptrl++=rgb16[*sptr];
    				*dptrl++=rgb16[*sptr];
   				
    				*dptrhm++=rgb16[*sptr];	
    				*dptrhm++=rgb16[*sptr];	
    				*dptrlm++=rgb16[*sptr];
    				*dptrlm++=rgb16[*sptr];
    				
    				sptr++;
			}
			dptrh+=SCREENWIDTH*6;
			dptrhm+=SCREENWIDTH*6;
			dptrlm+=SCREENWIDTH*6;
			dptrl+=SCREENWIDTH*6;
		}
	}
	else if(multiply==3)
	{
		unsigned short *dptrh=dptr;
		unsigned short *dptrm=dptrh+SCREENWIDTH*3;
		unsigned short *dptrl=dptrm+SCREENWIDTH*3;
		for(y=0;y<SCREENHEIGHT;y++)
		{
			for(x=0;x<SCREENWIDTH;x++)
			{
    				*dptrh++=rgb16[*sptr];	
    				*dptrh++=rgb16[*sptr];	
    				*dptrh++=rgb16[*sptr];	
    				*dptrl++=rgb16[*sptr];
    				*dptrl++=rgb16[*sptr];
    				*dptrl++=rgb16[*sptr];
    				*dptrm++=rgb16[*sptr];	
    				*dptrm++=rgb16[*sptr];	
    				*dptrm++=rgb16[*sptr];	
    				sptr++;
			}
			dptrh+=SCREENWIDTH*6;
			dptrm+=SCREENWIDTH*6;
			dptrl+=SCREENWIDTH*6;
		}
	}
	else if(multiply==2)
	{
		unsigned long *dptrh=(unsigned long *)dptr;
		unsigned long *dptrl=dptrh+SCREENWIDTH;
		for(y=0;y<SCREENHEIGHT;y++)
		{
			for(x=0;x<SCREENWIDTH;x++)
			{
    				*dptrh++=rgb16[*sptr];	
    				*dptrl++=rgb16[*sptr];
    				sptr++;
			}
			dptrh+=SCREENWIDTH;
			dptrl+=SCREENWIDTH;
		}
	}
	else
	{    	
    		for(y=0;y<X_height;y++)
    		{
    			for(x=0;x<X_width;x++)
    			{
    				*dptr++=rgb16[*sptr++];	
    			}
    		}
    	}
    }
    /*
     *	Blast out the image - the game can then run on until the
     *	start of the next drawing.
     */
    if(drawing_gc && drawing_image)
    {
    	gdk_draw_image(drawing_window->window, drawing_gc, drawing_image,
    		0, 0, 0, 0, X_width, X_height);
    }
    while (gtk_events_pending())
    	gtk_main_iteration();
}


//
// I_ReadScreen
//

void I_ReadScreen (byte* scr)
{
    memcpy (scr, screens[0], SCREENWIDTH*SCREENHEIGHT);
}


//
// Palette stuff.
//



void UploadNewPalette(byte *palette)
{
	int i;
	for(i=0;i<256;i++)
	{
		int r=gammatable[usegamma][*palette++]>>3;
		int g=gammatable[usegamma][*palette++]>>2;
		int b=gammatable[usegamma][*palette++]>>3;
		unsigned short rgb = (r<<11)|(g<<5)|b;
		
		/* Store in both words to speed the doubler */
		rgb16[i] = rgb<<16|rgb;
	}
	/* FIXME - compute RGB24, RGB32 and 8bit palettes */
}

//
// I_SetPalette
//
void I_SetPalette (byte* palette)
{
    UploadNewPalette(palette);
}

static gint paint_event(GtkWidget *self, GdkEvent *event)
{
	switch(event->type)
	{
		case GDK_CONFIGURE:
			if(drawing_image==NULL)
			{
				drawing_image=gdk_image_new(GDK_IMAGE_FASTEST,
					gdk_window_get_visual(self->window),
					X_width, X_height);
			}
			break;
		case GDK_EXPOSE:
			if(drawing_gc==NULL)
			{
//				drawing_gc=gdk_gc_new(drawing_window->window);
				drawing_gc=drawing_window->style->fg_gc[GTK_STATE_NORMAL];
				running=1;
				g_print("Expose received\n");
			}
			break;
		case GDK_KEY_PRESS:
		{
			event_t doom_event;
			doom_event.type = ev_keydown;
			doom_event.data1 = xlatekey(event);
			D_PostEvent(&doom_event);
			break;
		}
		case GDK_KEY_RELEASE:
		{
			event_t doom_event;
			doom_event.type = ev_keyup;
			doom_event.data1 = xlatekey(event);
			D_PostEvent(&doom_event);
			break;
		}
		default:
			break;
	}
	return FALSE;
}

void send_key(int k)
{
	event_t event;
	event.type = ev_keydown;
	event.data1 = k;
	D_PostEvent(&event);
	event.type = ev_keyup;
	event.data1 = k;
	D_PostEvent(&event);
}

void send_pause(GtkWidget *w, gpointer junk)
{
	send_key(KEY_PAUSE);
}

void send_f1(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F1);
}

void send_f2(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F2);
}

void send_f3(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F3);
}

void send_f4(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F4);
}

void send_f5(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F5);
}

void send_f6(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F6);
}

void send_f7(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F7);
}

void send_f8(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F8);
}

void send_f9(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F9);
}

void send_f10(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F10);
}

void send_f11(GtkWidget *w, gpointer junk)
{
	send_key(KEY_F11);
}

void send_tab(GtkWidget *w, gpointer junk)
{
	send_key(KEY_TAB);
}
void send_plus(GtkWidget *w, gpointer junk)
{
	send_key('+');
}
void send_minus(GtkWidget *w, gpointer junk)
{
	send_key('-');
}

void quit_program(GtkWidget *w, gpointer data)
{
	exit(0);
}

static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_ITEM("Save", "Save game", send_f6, NULL),
	GNOMEUIINFO_ITEM("Save as...", "Save game under a new name", send_f2, NULL),
	GNOMEUIINFO_ITEM("Load...", "Load game", send_f3, NULL),
	GNOMEUIINFO_ITEM("Restore", NULL, send_f9, NULL),
	GNOMEUIINFO_ITEM("End game", NULL, send_f7, NULL),
	GNOMEUIINFO_ITEM("Pause", "Pause game", send_pause, NULL),
	GNOMEUIINFO_ITEM("Exit", "Quit game", send_f10, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[]=
{
	GNOMEUIINFO_ITEM("About...", NULL, send_f1, NULL),
	GNOMEUIINFO_END
};


static GnomeUIInfo menu_table[]=
{
  GNOMEUIINFO_SUBTREE("File", file_menu),
  GNOMEUIINFO_SUBTREE("Help", help_menu),
  GNOMEUIINFO_END
};

static GnomeUIInfo toolbar_table[]=
{
  GNOMEUIINFO_ITEM("Map", "Toggle map mode", send_tab, NULL),
  GNOMEUIINFO_ITEM("Bigger", "Make view larger", send_plus, NULL),
  GNOMEUIINFO_ITEM("Smaller", "Make view smaller", send_minus, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM("Messages", "Toggle messages", send_f11, NULL),
  GNOMEUIINFO_ITEM("Detail", "Adjust picture detail", send_f11, NULL),
  GNOMEUIINFO_ITEM("Sound", "Adjust sound volume", send_f11, NULL),
  GNOMEUIINFO_ITEM("Gamma", "Adjust picture gamma", send_f11, NULL),
  GNOMEUIINFO_END
};

static error_t
parse_an_arg (int key, char *arg, struct argp_state *state)
{
	switch (key){
	case '2':
		multiply = 2;
		break;
	case '3':
		multiply = 3;
		break;
	case '4':
		multiply = 4;
		break;
	case 'g':
		// check if the user wants to grab the mouse (quite unnice)
		grabMouse = 1;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp_option argp_options [] = {
	{ "double",     '2', NULL,     0, N_("double pixels"), 0 },
	{ "triple",   	'3', NULL,     0, N_("triple pixels"),   0 },
	{ "quad",     	'4', NULL,     0, N_("4x display"),      0 },
	{ "grabmouse",  'g', NULL,     0, N_("Grab the mouse"),  0 },
	{ NULL, 0, NULL, 0, NULL, 0 },
};

static struct argp parser =
{
	argp_options, parse_an_arg, NULL, NULL, NULL, NULL, NULL
};

void I_InitGraphics(void)
{

    char*		d;
    int			n;
    int			pnum;
    int			x=0;
    int			y=0;
    
    // warning: char format, different type arg
    char		xsign=' ';
    char		ysign=' ';

    int foo;    
    int			oktodraw;
    unsigned long	attribmask;
    int			valuemask;
    static int		firsttime=1;
    
    GtkWidget		*vbox;

    if (!firsttime)
	return;
    firsttime = 0;
    
    
    gnome_init("gnoom", &parser, myargc, myargv, 0, NULL);
    

    signal(SIGINT, (void (*)(int)) I_Quit);

    X_width = SCREENWIDTH * multiply;
    X_height = SCREENHEIGHT * multiply;

    if(gdk_visual_get_best_depth()!=16)
    {
	I_Error("gnomedoom currently only supports 16bit screens");
    }

//    X_cmap = XCreateColormap(X_display, RootWindow(X_display,
//						   X_screen), X_visual, AllocAll);

    main_window = gnome_app_new("doom", "Doom");
    
    gtk_window_set_policy(GTK_WINDOW(main_window), FALSE, FALSE, TRUE);
    
    gnome_app_create_menus(GNOME_APP(main_window), menu_table);
    gtk_menu_item_right_justify(GTK_MENU_ITEM(menu_table[1].widget));

	/*
	 *	Toolbar is too broken to use, it steals some events,
	 *	lets others through in error, uses an overlarge font
	 *	and is a fuckup. Never mind gnome .12 will no doubt
	 *	be a lot better.
	 */
/*  gnome_app_create_toolbar(GNOME_APP(main_window), toolbar_table); */
    
    gtk_signal_connect(GTK_OBJECT(main_window), "delete_event", gtk_main_quit, NULL);
    gtk_signal_connect(GTK_OBJECT(main_window), "destroy", gtk_main_quit, NULL);
    
    

    vbox = gtk_vbox_new(FALSE, 0);    
    
    gnome_app_set_contents(GNOME_APP(main_window), vbox);
    
    
    drawing_window = gtk_drawing_area_new();
    gtk_drawing_area_size(drawing_window, X_width, X_height);
//    gtk_widget_set_usize(drawing_window, X_width, X_height);
    
    gtk_widget_set_events(drawing_window, GDK_ALL_EVENTS_MASK);
    gtk_signal_connect(GTK_OBJECT(drawing_window), "event", 
    				(GtkSignalFunc)paint_event, 0);
    /* Apparently we can't set the focus on the drawing_window so
       it gets the key events, so we just grab the ones from the main
       window */
    gtk_signal_connect(GTK_OBJECT(main_window), "key_press_event", 
    				(GtkSignalFunc)paint_event, 0);
    gtk_signal_connect(GTK_OBJECT(main_window), "key_release_event", 
    				(GtkSignalFunc)paint_event, 0);
    gtk_window_set_focus(main_window, drawing_window);
    gtk_widget_show(drawing_window);
    gtk_container_add(GTK_CONTAINER(vbox), drawing_window);

    gtk_widget_show(vbox);

    gtk_widget_show(main_window);        

//   XDefineCursor(X_display, X_mainWindow,
//		  createnullcursor( X_display, X_mainWindow ) );

    screens[0] = (unsigned char *) malloc (SCREENWIDTH * SCREENHEIGHT);

}


unsigned	exptable[256];

void InitExpand (void)
{
    int		i;
	
    for (i=0 ; i<256 ; i++)
	exptable[i] = i | (i<<8) | (i<<16) | (i<<24);
}

double		exptable2[256*256];

void InitExpand2 (void)
{
    int		i;
    int		j;
    // UNUSED unsigned	iexp, jexp;
    double*	exp;
    union
    {
	double 		d;
	unsigned	u[2];
    } pixel;
	
    printf ("building exptable2...\n");
    exp = exptable2;
    for (i=0 ; i<256 ; i++)
    {
	pixel.u[0] = i | (i<<8) | (i<<16) | (i<<24);
	for (j=0 ; j<256 ; j++)
	{
	    pixel.u[1] = j | (j<<8) | (j<<16) | (j<<24);
	    *exp++ = pixel.d;
	}
    }
    printf ("done.\n");
}

int	inited;

void
Expand4
( unsigned*	lineptr,
  double*	xline )
{
    double	dpixel;
    unsigned	x;
    unsigned 	y;
    unsigned	fourpixels;
    unsigned	step;
    double*	exp;
	
    exp = exptable2;
    if (!inited)
    {
	inited = 1;
	InitExpand2 ();
    }
		
		
    step = 3*SCREENWIDTH/2;
	
    y = SCREENHEIGHT-1;
    do
    {
	x = SCREENWIDTH;

	do
	{
	    fourpixels = lineptr[0];
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff0000)>>13) );
	    xline[0] = dpixel;
	    xline[160] = dpixel;
	    xline[320] = dpixel;
	    xline[480] = dpixel;
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff)<<3 ) );
	    xline[1] = dpixel;
	    xline[161] = dpixel;
	    xline[321] = dpixel;
	    xline[481] = dpixel;

	    fourpixels = lineptr[1];
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff0000)>>13) );
	    xline[2] = dpixel;
	    xline[162] = dpixel;
	    xline[322] = dpixel;
	    xline[482] = dpixel;
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff)<<3 ) );
	    xline[3] = dpixel;
	    xline[163] = dpixel;
	    xline[323] = dpixel;
	    xline[483] = dpixel;

	    fourpixels = lineptr[2];
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff0000)>>13) );
	    xline[4] = dpixel;
	    xline[164] = dpixel;
	    xline[324] = dpixel;
	    xline[484] = dpixel;
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff)<<3 ) );
	    xline[5] = dpixel;
	    xline[165] = dpixel;
	    xline[325] = dpixel;
	    xline[485] = dpixel;

	    fourpixels = lineptr[3];
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff0000)>>13) );
	    xline[6] = dpixel;
	    xline[166] = dpixel;
	    xline[326] = dpixel;
	    xline[486] = dpixel;
			
	    dpixel = *(double *)( (int)exp + ( (fourpixels&0xffff)<<3 ) );
	    xline[7] = dpixel;
	    xline[167] = dpixel;
	    xline[327] = dpixel;
	    xline[487] = dpixel;

	    lineptr+=4;
	    xline+=8;
	} while (x-=16);
	xline += step;
    } while (y--);
}


